#include "zoo.h"
/*
	Hayden Chalin
	CS 163
	Program 1
	04/07/2024
	The purpose of this program is to write classes to manage a zoo's database of animals 
	using OOP.  There will be 2 classes, one that will manage the entire zoo of animals, and 
	another that will represen a animal...
*/

/*
	 function prototypes
 */

int display_menu(int & menu_choice); 
void display_animals_in_breed(CS_Zoo & zoo);
void display_breeds(CS_Zoo & zoo); // displays all the animals in a breed
void add_breed_to_zoo(CS_Zoo & zoo);
void add_animal_to_breed(CS_Zoo & zoo);
void display_in_range(CS_Zoo & zoo);
void remove_breed(CS_Zoo & my_zoo);


int main(){

	// Menu Choice
	int menu_choice {0};
	
	// CS Zoo
	CS_Zoo my_zoo; // creat the zoo instance

	// Menu loop
	while (menu_choice != QUIT){
		// Get menu choice
		display_menu(menu_choice);
		
		// Control filter	
		switch (menu_choice){
			case ADD_ANIMAL:
				cout << "ADD ANIMAL" << endl;
				add_animal_to_breed(my_zoo);
				break;
			case ADD_BREED:
				cout << "ADD BREED" << endl;
				add_breed_to_zoo(my_zoo);
				break;
			case REMOVE_BREED:
				cout << "REMOVE BREED" << endl;
				remove_breed(my_zoo);
				break;
			case DISPLAY_BREEDS:
				cout << "DISPLAY BREEDS" << endl;
				display_breeds(my_zoo); // displays all the animals in a breed
				break;
			case DISPLAY_ANIMALS_IN_BREED:
				cout << "DISPLAY_ANIMALS_IN_BREED" << endl;
				display_animals_in_breed(my_zoo);	
				break;
			case DISPLAY_IN_RANGE:
				cout << "DISPLAY IN RANGE" << endl;
				display_in_range(my_zoo);
				break;
			case QUIT:
				cout << "QUIT" << endl;
				break;
		}
	}
	return 0;

}

int display_menu(int & menu_choice){
	/*
		This function will display the menu and update the menu choice.

		@param & menu_Choice: this menu choice controls the menu loop.
	*/
	// Print menu
	cout << "Menu: " << endl;
	cout << "1) Add zoo animal" << endl;	
	cout << "2) Display animals in a breed" << endl;	
	cout << "3) Display all breeds" << endl;
	cout << "4) Add breed" << endl;
	cout << "5) Display in age range" << endl;
	cout << "6) Remove a breed" << endl;
	cout << "9) Quit" << endl;	
	cout << "Choice: ";
	cin >> menu_choice;
	cin.ignore(100, '\n');
	
	// success 
	return 1;
}

void add_breed_to_zoo(CS_Zoo & my_zoo){
	/*
		This function will add a breed to the zoo
		@param my_Zoo: instance of the CS_Zoo class
		@returns int: represents the succes/failed state
	*/
	// create the breed data	
	breed_data data;
	char temp_breed_name[51];
	char temp_desc[300];	
	char temp_habitat_overview[300];

	// name
	cout << endl;
	cout << "enter the name of the new breed: ";
	cin.get(temp_breed_name, 51, '\n');
	cin.ignore(100, '\n');
	data.breed_name = new char[strlen(temp_breed_name) + 1];
	strcpy(data.breed_name, temp_breed_name);
	
	if (my_zoo.does_breed_exist(data.breed_name)){
		cout << "This breed alread exists, try adding to it" << endl;
		return;
	}
	
	// breed description
	cout << "enter the breed description: ";
	cin.get(temp_desc, 300, '\n');
	cin.ignore(100, '\n');
	data.description = new char[strlen(temp_desc) + 1];
	strcpy(data.description, temp_desc);
	
	// habitat
	cout << "enter the habitat description: ";
	cin.get(temp_habitat_overview, 300, '\n');
	cin.ignore(100, '\n');
	data.habitat_overview = new char[strlen(temp_habitat_overview) + 1];
	strcpy(data.habitat_overview, temp_habitat_overview);
	
	// avg life span		
	cout << "enter the average life span: ";
	cin >>data.avg_lifespan; cin.ignore(100, '\n');
	
	
	// send client data	
	switch (my_zoo.add_breed(data)){
		case 0:
			cerr << "breed could not be added" << endl;
			break;
		case 1:
			cout << "breed has been added" << endl;
			break;
		case 2: 
			cout << "this breed already exists" << endl;
			break;
	}

}


void display_breeds( CS_Zoo & my_zoo){
/*
	this is a void function that displays all breeds in the zoo
	@param my_zoo: this is an instance of the zoo class
*/

	cout << endl;
	if (!my_zoo.display_all_breeds()){
		cerr << "there are no breeds to display" << endl;	

	}
}

void add_animal_to_breed(CS_Zoo & my_zoo){
	/*
		This function adds a animal to a specific breed by asking the user for a breed name 
		they want to add to and add the newly created node to the breed. 
		@param my_zoo: this is an instance of the zoo class
	*/
	char breed_name[50];
	animal_data data;	
	
	// Temp Variables
	char temp_name[50];
	char temp_color[20];
	char temp_origin[100];
		
	// Get the name of the breed to look for
	cout << endl;
	cout << "What breed would you like to add a animal too: ";
	cin.get(breed_name, 50, '\n'); cin.ignore(100, '\n');

	// check to see if it doesnt exist
	//	XXX this could be done more efficiently
	if (!my_zoo.does_breed_exist(breed_name)){
		cout << "this breed doesnt exist! here are the current breeds in the zoo: " << endl;	
		my_zoo.display_all_breeds();
		return; // stop adding
	}
	
	// Get the animal name to add	
	cout << "Enter the name of the animal: ";
	cin.get(temp_name, 50, '\n'); cin.ignore(100, '\n');
	data.animal_name = new char[strlen(temp_name) + 1];
	strcpy(data.animal_name, temp_name);
	
	// get color
	cout << "Enter the color of the animal: ";
	cin.get(temp_color, 50, '\n'); cin.ignore(100, '\n');
	data.color = new char[strlen(temp_color) + 1];
	strcpy(data.color, temp_color);

	// get origin
	cout << "Enter the origin of the animal: ";
	cin.get(temp_origin, 100, '\n'); cin.ignore(100, '\n');
	data.origin = new char[strlen(temp_origin) + 1];
	strcpy(data.origin, temp_origin);

	// get captive years
	cout << "Enter the number of captive years: ";
	cin >> data.captive_years; cin.ignore(100, '\n');
	// get age 
	cout << "Enter the animals age: ";
	cin >> data.age; cin.ignore(100, '\n');

	// send client data
	
	switch (my_zoo.add_animal(breed_name, data)){
		case 0: 
			// Technically this will never be reached??
			cout << "The zoo is currently empty, try adding a breed first" << endl;
			break;
		case 1: 
			cout << "The animal has been added!" << endl;
			break;
		case 3: 
			cout << "The breed was not found after trying to add." << endl;
			break;
	}


}

void display_animals_in_breed(CS_Zoo & my_zoo){
	/*
		This function will ask the user to type in a breed the display all animals in that breed,
		if the breed the user enters does not exist it will notify the user
		@param my_zoo: this is the instance of the CS_Zoo class
	*/
	
	// Name of the breed to look for
	char breed_name[50];
		
	// Get the name of the breed to look for
	cout << endl;
	cout << "What breed would you like to display: ";
	cin.get(breed_name, 50, '\n'); cin.ignore(100, '\n');

	// check to see if it doesnt exist
	//	XXX this could be done more efficiently
	if (!my_zoo.does_breed_exist(breed_name)){
		cout << "this breed doesnt exist! here are the current breeds in the zoo: " << endl;	
		my_zoo.display_all_breeds();
		return; // stop display
	}
		
	// call the backend ADT to display	
	switch (my_zoo.display_animals_in_breed(breed_name)){
			case 0:
				cerr << "Something went wrong with the display" << endl;	
				break;
			case 2:
				cout << "There are no animals in this breed yet, try adding one!" << endl;
				break;
		}
}


void display_in_range(CS_Zoo & my_zoo){
	/*
		this function will display all animals in a particular age range
		@param my_zoo: this is the instance of the CS_Zoo class
	*/

	int min_age, max_age;
	pair<int, int> age_range;
	
	// get the min age
	cout << "What is the lower age limit: ";
	cin >> min_age; cin.ignore(100, '\n');
	cout << endl;
	
	
	// get the max age
	cout << "What is the max age limit: ";
	cin >> max_age; cin.ignore(100, '\n');
	cout << endl;
	
	// make the vector pair
	// age_range.push_back(make_pair(min_age, max_age));
	age_range = make_pair(min_age, max_age);

	switch(my_zoo.display_age_range(age_range)){
		case 0:
			cout << "There are no animals in that age range" << endl;
			break;
		case 1: 
			cout << "There are no animals to display" << endl;
			break;
		case 2: 
			cout << "All animals in the range have been displayed" << endl;
			break;
		case 3:
			cout << "Max age must be greater than or equal to the min age" << endl;	
			break;	
	}	


}


void remove_breed(CS_Zoo & my_zoo){
	/*
		This function will remove a breed from the zoo				
		@param my_zoo: this is the instance of the CS_Zoo class
	*/

	char breed_to_remove[50];
	
	cout << "Please enter the name of the breed to remove: ";
	cin.get(breed_to_remove, 50, '\n'); cin.ignore(50, '\n');

 switch (my_zoo.remove_breed(breed_to_remove)){
		case 0:
			cout << "There is nothing to remove" << endl;
			break;
		case 1: 
			cout << "The breed you are trying delete doesnt exist" << endl;
			break;
		case 2:
			cout << "The breed has been removed!" << endl;
			break;
	}

}
