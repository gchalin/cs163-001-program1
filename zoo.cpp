#include "zoo.h"
/*
	Hayden Chalin
	CS 163
	Program 1
	04/07/2024
	The purpose of this program is to write classes to manage a zoo's database of animals 
	using OOP.  There will be 2 classes, one that will manage the entire zoo of animals, and 
	another that will represen a animal...
*/

CS_Zoo::CS_Zoo(){
	// This is the constructor of the zoo
	b_head = nullptr; // set the breed head to nullptr;	
}



CS_Zoo::~CS_Zoo(){
	/*
		This is the deconstructor
	*/	
	if (!b_head) return;
	destroy_program(b_head);
}

void CS_Zoo::destroy_program(breed_node * b_head){
	// end of breed list
	if (!b_head) return;
	
	if (b_head->a_head){
		animal_node * curr = b_head->a_head;
		while (curr){
		delete [] curr->data.animal_name;
		delete [] curr->data.color;
		delete [] curr->data.origin;
		curr = curr->next;


	}
		curr = nullptr;
	}
	delete [] b_head->data.breed_name;
	delete [] b_head->data.description;
	delete [] b_head->data.habitat_overview;
	delete  b_head;
	delete b_head->a_head;
	destroy_program(b_head->next);
	b_head->next = nullptr;


}

/*
*************
	ADD ANIMAL
*************
*/

int CS_Zoo::add_breed(breed_data & in_data, breed_node * & b_head ){ 
	/*
		 This function will add a breed node along with the required information to the LLL
		 DS.
		 @return int: this will represent the sucess and failed state of the fn.

	 */

	if (!b_head){
		b_head = new breed_node;
		b_head->next = nullptr;
		b_head->data = in_data; 		
		return 1;	
	}

	if (strcmp(b_head->data.breed_name, in_data.breed_name) == 0){
		return 2;
	}

	return add_breed(in_data, b_head->next);
}

int CS_Zoo::add_breed(breed_data & in_data){
	/*
		This function will get the breed in_Data from the client and pass it to the private section
		to add a node to the LLL
		@param data: this is a breed struct obtained from the client
		@returns int: This is the success/failed state of the function
	*/
	if (!b_head){
		cout << "FIRST ADD" << endl;
		b_head = new breed_node;
		b_head->data = in_data;
		b_head->next = nullptr;
		return 1;	
	}
	// this will compare the first and data to be added
	if (strcmp(b_head->data.breed_name, in_data.breed_name) == 0){
		return 2;
	}

	// start recursion
	return add_breed(in_data, b_head->next);
}
/* 
************
	DISPLAY ALL BREEDS 
*************
*/
int CS_Zoo::display_all_breeds(breed_node * b_head, int idx){
/*
	RECURSIVE CALL OF DISPLAY ALL BREED
*/
	if (!b_head) return 1;	
	cout << "*******" << endl;
	cout << idx << ")" << endl;
	cout << "Breed Name: " << b_head->data.breed_name << endl;	
	cout << "breed description: " << b_head->data.description << endl;	
	cout << "breed habitat description: " << b_head->data.habitat_overview << endl;	
	cout << "average lifespan: " << b_head->data.avg_lifespan << endl;
	display_all_breeds(b_head->next, ++idx);
	return 1;
}

int CS_Zoo::display_all_breeds(){// todo
	/*
		THIS WILL DISPLAY ALL THE BREEDS
	*/

	if (!b_head){
		return 0;	
	}
		
	return display_all_breeds(b_head);
}

/*
*************
	REMOVE BREED	
*************
*/

int CS_Zoo::remove_breed(char breed_to_remove[], breed_node * & b_head){
	if (!b_head->next) return 2;
	// get to the end			
	 remove_breed(breed_to_remove, b_head->next);
	//  find the matching breed name
	if (strcmp(b_head->next->data.breed_name, breed_to_remove) == 0){
			breed_node * hold = b_head->next->next;	

			// delete the animals first
			if (b_head->a_head) delete_animals(b_head->a_head);
		
			delete [] b_head->next->data.breed_name;
			delete [] b_head->next->data.description;
			delete [] b_head->next->data.habitat_overview;
			delete b_head->next;
			b_head->next->next = nullptr;
			b_head->next = hold;
			return 2;
	}				
	return 1;
}
/*
	TODO: The remove function needs to be completed. make the deletion of the animal list
				a function. And check for the base cases in the wrapper function!
*/
int CS_Zoo::remove_breed(char breed_to_remove[]){ 
	// if there is no breeds
	if (!b_head) return 0;

	// if there is one node
	if (!b_head->next ){
		if ( strcmp(b_head->data.breed_name, breed_to_remove) == 0){
			if (b_head->a_head) delete_animals(b_head->a_head); 

			delete [] b_head->data.breed_name;
			delete [] b_head->data.description;
			delete [] b_head->data.habitat_overview;
			delete b_head;
			b_head = nullptr;	

			return 2;
		}
	}

	// if deleting the first node
	if ( strcmp(b_head->data.breed_name, breed_to_remove) == 0){
			if (b_head->a_head) delete_animals(b_head->a_head);
			breed_node * hold = b_head->next;
			delete [] b_head->data.breed_name;
			delete [] b_head->data.description;
			delete [] b_head->data.habitat_overview;
			delete b_head;
			b_head = hold;
			return 2;
	}

	// start recursion	
	return	remove_breed(breed_to_remove, b_head);	
}

// helper for delte breed, the return type is handled in the parent function
void CS_Zoo::delete_animals(animal_node * a_head){
	if (!a_head) return;
		
	return delete_animals(a_head->next); // get to the end
	// delete as the stack frames pop off
	delete [] a_head->data.animal_name;
	delete [] a_head->data.color;
	delete [] a_head->data.origin;
	a_head->next = nullptr;
	delete a_head;

}

/*
*************
	ADD ANIMAL
*************
*/

	
int CS_Zoo::add_animal(char name_of_breed[], animal_data & in_data, breed_node * & b_head){
	// TODO make sure this adds in alphabetical order!

	if (!b_head)	return 3; // the breed was never found (this should never hit)

	if (strcmp(name_of_breed, b_head->data.breed_name) == 0){
		// this is the location of the animal node head that matches

		if (!b_head->a_head){
			b_head->a_head = new animal_node;
			b_head->a_head->data = in_data;
			b_head->a_head->next = nullptr;
			return 1; // animal node has been created, and data is added
		}	
	
		// handle adding animal if there is only one
		if (!b_head->a_head->next){
			if(strcmp(b_head->a_head->data.animal_name, in_data.animal_name) > 0){
				animal_node * temp = new animal_node;
				temp->data = in_data;	
				temp->next = b_head->a_head;
				b_head->a_head = temp;
				return 1;	
			}
		}
		// Iterave section	
		animal_node * curr = b_head->a_head;
		
		while (curr->next && strcmp(curr->next->data.animal_name, in_data.animal_name) < 0 ){
			curr= curr->next;
		}
		// the new data now needs to be at currs next
		animal_node * temp = new animal_node;
		temp->data = in_data;

		// logical connections, curr could be at the end or somewhere in the middle, we dont know
		if (curr->next){
			temp->next = curr->next;	
		}	else {
			temp->next = nullptr;	
		}
		
		curr->next = temp;
		return 1;
	}
	return add_animal(name_of_breed, in_data, b_head->next);

}; 

	
int CS_Zoo::add_animal(char name_of_breed[],  animal_data & in_data){ 
	if (!b_head)	return 0; // The zoo is empty
	return add_animal(name_of_breed, in_data, b_head);
};

/*
*******************
  DOES BREED EXIST
*******************
*/
int CS_Zoo::does_breed_exist(char breed_name[]){

	if(!b_head) return 0; // there is no head, breed does not exist
	breed_node * curr = b_head;
	while(curr){
		if (strcmp(breed_name, curr->data.breed_name) == 0){
			return 1; // the breed exists
		}

		curr = curr -> next;
	}	
	return 0; // not found
}
/*
*************
	DISPLAY ANIMALS IN A BREED
*************
*/
int CS_Zoo::display_animals_in_breed(char breed_name[], breed_node * & b_head ){

	if (!b_head) return 1; // end of display

	if (strcmp(b_head->data.breed_name, breed_name) == 0){
			// This is the head of the animal list
			if (!b_head->a_head) return 2;

			int idx {1};
			animal_node * curr = b_head->a_head;
				
			while (curr){
				// perform the display for each animal in the breed head
				cout << idx << ")*************************" << endl;
				cout << "Animal Name: " << curr->data.animal_name << endl;
				cout << "Animal color: " << curr->data.color << endl;
				cout << "Origin: " << curr->data.origin << endl;
				cout << "Captive years: " << curr->data.captive_years << endl;
				cout << "Animal age: " << curr->data.age << endl;
				cout << endl;
				++idx;
				curr = curr->next;

			}
	}

		
return display_animals_in_breed(breed_name, b_head->next);
}

int CS_Zoo::display_animals_in_breed(char breed_name[]){
	if (!b_head) return 0;

	// Start the recursive display
	return display_animals_in_breed(breed_name, b_head);
} 




/*
 *************
 DIPSLAY IN RANGE
 *************
 */
int CS_Zoo::display_age_range(pair<int, int> & age_range, breed_node * & b_head, int idx){ 

	if (!b_head) return 0; // end of display

	// no animals in this breed, go to next
	if (!b_head->a_head) return display_age_range(age_range, b_head->next);

	animal_node * curr = b_head->a_head;

	while (curr){
		if (curr->data.age >= age_range.first && curr->data.age <= age_range.second){
			// animal is in age range
			cout << idx << ")*************************" << endl;
			cout << "Breed: " << b_head->data.breed_name << endl;
			cout << "Animal Name: " << curr->data.animal_name << endl;
			cout << "Animal color: " << curr->data.color << endl;
			cout << "Origin: " << curr->data.origin << endl;
			cout << "Captive years: " << curr->data.captive_years << endl;
			cout << "Animal age: " << curr->data.age << endl;
			cout << endl;
			++idx;
		}
		curr = curr->next;
	return 1;

	}
	return display_age_range(age_range, b_head->next, idx);
}; 
int CS_Zoo::display_age_range(pair<int, int> & age_range){
	
	if (!b_head) return 1;
	if (age_range.first > age_range.second) return 3;
	return display_age_range(age_range, b_head);
}; 
