/*
	Hayden Chalin
	CS 163
	Program 1
	04/07/2024
	The purpose of this program is to write classes to manage a zoo's database of animals 
	using OOP.  There will be 2 classes, one that will manage the entire zoo of animals, and 
	another that will represen a animal
*/

#include <iostream>
#include <cctype>
#include <cstring>
#include <utility> // Used for std::pair
#include <vector>

using namespace std;

/*
	MENU CHOICES
*/

const int ADD_ANIMAL {1};
const int DISPLAY_ANIMALS_IN_BREED {2};
const int DISPLAY_BREEDS {3};
const int ADD_BREED {4};
const int DISPLAY_IN_RANGE {5};
const int REMOVE_BREED {6};
const int QUIT {9};
/* 
				  DATA STRUCTURE
					
				---CS_ZOO CLASS---
				__________________
		
		[head] -> [breed] -> [breed] -> [breed]
								| 
								v
	 [head] -> [animal]
								|
								v
						 [animal]


*/

/*
	NODES / STRUCTS
*/
// ZOO ANIMAL
struct animal_data { 
	char * animal_name; // pointer to the animals name
	char * color; // pointer to the color of the animal
	char * origin; //pointer to the origin of the animal (where they came from)
	int captive_years; 
	int age; 

};

// animal node
struct animal_node {
	animal_data  data; 
	animal_node * next; // pointer to the next animal in the lll
};

//animal breed
struct breed_data {
	char * breed_name; 
	char * description; 
	char * habitat_overview; 
	int avg_lifespan;
};

// breed node
struct breed_node {
	breed_data data; 

	// lll pointers
	breed_node * next; 
	animal_node * a_head; 
};

/*
	 CLASSES
*/
// Zoo
class CS_Zoo {
	// This class will manage the entire zoo 

	private: 	
							breed_node * b_head; // pointer to the head of the breed lll
							//breed_node * b_tail;  

							int add_breed(breed_data & data, breed_node * & b_head);
							int display_all_breeds(breed_node *  b_head, int idx = 1);
							int display_animals_in_breed(char name_of_breed[], breed_node * & b_head); //todo consider making the char a pointer
							int remove_breed(char breed_to_remove[], breed_node * & b_head); // name of the breed to remove; 
							int add_animal(char name_of_breed[] , animal_data & in_data, breed_node * & b_head); // add the new animal node to the lll
							int display_age_range(pair<int, int> & age_range, breed_node * & b_head, int idx = 1);
							// helper functions for destroying 
							void delete_animals(animal_node * a_head);
							void destroy_program(breed_node * b_head);

	public: //XXX Do not pass any node pointers to public member functions, define a fn over load in the private! XXX
					// Data passed to these functions should be obtained from the client

							CS_Zoo(); // constructor
							~CS_Zoo(); // deconstructor
							int add_breed(breed_data & data);
							int display_all_breeds();
							int display_animals_in_breed(char name_of_breed[]); // this will take the name of a breed to search for
							int remove_breed(char breed_to_remove[]); // name of the breed to remove
							int add_animal(char name_of_breed[], animal_data & in_data);
							int does_breed_exist(char name_of_breed[]);
							int display_age_range(pair<int, int> & age_range); //ex: pair<int, int> age_range = {23, 45}; age_range.first etc... 
};




